(function () {
    'use strict';

    angular
        .module('facebookToolFrontApp')
        .controller('NavbarController', NavbarController);

    NavbarController.$inject = ['$state', 'Auth', 'Principal', 'ProfileService', 'LoginService', '$scope', '$localStorage'];

    function NavbarController($state, Auth, Principal, ProfileService, LoginService, $scope, $localStorage) {
        var vm = this;

        vm.isNavbarCollapsed = true;
        vm.isShowNav = true;
        vm.account = '';
        vm.isAuthenticated = Principal.isAuthenticated;


        vm.login = login;

        
        function login() {
            FB.login(function (response) {
                if (response.authResponse) {

                    var data = response.authResponse;

                    $localStorage.access_token = data.accessToken;
                    $localStorage.user_iD = data.userID;
                    
                    $state.go('pages', {}, {reload: true});

                } else {
                    console.log('User cancelled login or did not fully authorize.');
                }
            },'public_profile,email,pages_messaging,pages_show_list,user_events,pages_manage_cta,user_photos,manage_pages');
        }


    }
})();
