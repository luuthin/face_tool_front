(function () {
    'use strict';

    angular
        .module('facebookToolFrontApp')
        .factory('Register', Register);

    Register.$inject = ['$resource'];

    function Register ($resource) {
        return $resource('api/register', {}, {});
    }
})();
