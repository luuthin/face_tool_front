(function() {
    'use strict';

    angular
        .module('facebookToolFrontApp')
        .factory('InboxService', InboxService);

    InboxService.$inject = ['$resource'];

    function InboxService ($resource) {
        var service = $resource('management/inbox/:id', {}, {
            'get': {
                method: 'GET',
                isArray: true
            },
            'query': {
                method: 'GET',
                isArray: true,
                params: {fromDate: null, toDate: null}
            }
        });

        return service;
    }
})();
