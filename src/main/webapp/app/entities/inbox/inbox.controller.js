(function () {
    'use strict';

    angular
        .module('facebookToolFrontApp')
        .controller('InboxController', InboxController);

    InboxController.$inject = ['$filter', 'InboxService', '$state', '$localStorage', '$scope'];

    function InboxController($filter, InboxService, $state, $localStorage, $scope) {
        var vm = this;

        vm.commentClickDetail = commentClickDetail;

        vm.listCommentFb = []
        vm.listDetailCommentFb = [];
        vm.listCommentFbDetail = []
        vm.showDetailComment = showDetailComment;
        vm.imgPage = []

        vm.pageId = $localStorage.page_id;
        vm.access_token = $localStorage.access_token;
        vm.imgPage = $localStorage.page_img;


        getCommentFb();


        function showDetailComment() {
            console.log('getcoment detail');
        }

        function getCommentFb() {


            FB.api(
                vm.pageId + '/feed',
                'GET',
                {
                    access_token: vm.access_token,
                    fields: "comments{comments,message,from,created_time},message,created_time,updated_time"
                },
                function (response) {

                    var serializable = response.data

                    for (var i = 0; i < serializable.length - 1; i++) {
                        for (var j = i + 1; j < serializable.length; j++) {
                            if (Date.parse(window.moment(serializable[i].updated_time).format('MM/DD/YYYY HH:mm')) < Date.parse(window.moment(serializable[j].updated_time).format('MM/DD/YYYY HH:mm'))) //Tăng dần or Giảm dần a[i]<a[j]
                            {
                                var k = serializable[i];
                                serializable[i] = serializable[j];
                                serializable[j] = k;
                            }
                        }
                    }

                    for (var key in serializable) {
                        if (serializable[key].comments != '' && serializable[key].comments != null) {
                            for (var keys in serializable[key].comments.data) {
                                vm.listCommentFb.push(serializable[key].comments.data[keys])
                                vm.listDetailCommentFb.push(serializable[key].comments.data[keys])
                            }
                        }
                    }

                    for (var i in vm.listCommentFb) {
                        if (vm.listCommentFb[i].comments != '' && vm.listCommentFb[i].comments != null) {
                            for (var ii in vm.listCommentFb[i].comments.data) {
                                if (Number(ii) == vm.listCommentFb[i].comments.data.length - 1) {
                                    vm.listCommentFb[i] = vm.listCommentFb[i].comments.data[ii]
                                }
                            }
                        }
                    }


                    $scope.$apply(vm.listCommentFb)
                }
            );
        }

        function commentClickDetail(id, index) {
            vm.listCommentFbDetail = vm.listDetailCommentFb[index]
        }
    }
})();
