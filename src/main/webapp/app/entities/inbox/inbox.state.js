(function() {
    'use strict';

    angular
        .module('facebookToolFrontApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('inbox', {
            parent: 'entity',
            url: '/inbox',
            data: {
                // authorities: ['ROLE_ADMIN'],
                // pageTitle: 'inbox.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/inbox/inbox.html',
                    controller: 'InboxController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    // $translatePartialLoader.addPart('inbox');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
