(function() {
    'use strict';

    angular
        .module('facebookToolFrontApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('posts', {
            parent: 'entity',
            url: '/posts',
            data: {
                // authorities: ['ROLE_ADMIN'],
                // pageTitle: 'posts.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/posts/posts.html',
                    controller: 'PostsController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    // $translatePartialLoader.addPart('posts');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
