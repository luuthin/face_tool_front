(function () {
    'use strict';

    angular
        .module('facebookToolFrontApp')
        .controller('PostsController', PostsController);

    PostsController.$inject = ['$filter', 'PostsService', 'ParseLinks', '$localStorage', '$scope'];

    function PostsController($filter, PostsService, ParseLinks, $localStorage, $scope) {
        var vm = this;

        vm.pageId = $localStorage.page_id;
        vm.access_token = $localStorage.access_token;
        vm.listPostsFb = []


        FB.api(vm.pageId +'/feed',
        'GET',
        {
            access_token: vm.access_token,
            fields:"full_picture,message,created_time,comments"
        },
        function(response) {
            vm.listPostsFb = response.data;

            for (var i = 0; i < vm.listPostsFb.length; i++) {
                if (vm.listPostsFb[i].full_picture == '' || vm.listPostsFb[i].full_picture == null) {
                    vm.listPostsFb[i].full_picture = "../../content/images/NO_IMG.png"
                }
            }

            $scope.$apply(vm.listPostsFb);
        });
    }
})();