(function() {
    'use strict';

    angular
        .module('facebookToolFrontApp')
        .factory('PostsService', PostsService);

    PostsService.$inject = ['$resource'];

    function PostsService ($resource) {
        var service = $resource('management/posts/:id', {}, {
            'get': {
                method: 'GET',
                isArray: true
            },
            'query': {
                method: 'GET',
                isArray: true,
                params: {fromDate: null, toDate: null}
            }
        });

        return service;
    }
})();
