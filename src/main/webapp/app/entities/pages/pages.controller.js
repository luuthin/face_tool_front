(function () {
    'use strict';

    angular
        .module('facebookToolFrontApp')
        .controller('PagesController', PagesController);

    PagesController.$inject = ['$state', '$localStorage', '$scope'];

    function PagesController($state, $localStorage, $scope) {
        var vm = this;

        vm.goToPost = goToPost;
        vm.lstPage = []

        FB.api('/me/accounts?fields=features,picture,description,name,new_like_count', function (response) {
            vm.lstPage = angular.copy(response.data);
        });

        function goToPost(item){
            $localStorage.page_id = item.id;
            $localStorage.page_img = item.picture.data.url;
            $state.go('posts')
        }
    }
})();
