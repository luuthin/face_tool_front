(function() {
    'use strict';

    angular
        .module('facebookToolFrontApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('pages', {
            parent: 'entity',
            url: '/pages',
            data: {
                // authorities: ['ROLE_ADMIN'],
                // pageTitle: 'pages.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/pages/pages.html',
                    controller: 'PagesController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    // $translatePartialLoader.addPart('pages');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
