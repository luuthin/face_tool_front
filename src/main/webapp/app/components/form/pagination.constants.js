(function() {
    'use strict';

    angular
        .module('facebookToolFrontApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
